﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Tech_Labs.Pages
{
    public class IndexModel : PageModel
    {

        public string currentTime { get; set; }

        public void OnGet()
        {
            currentTime = DateTime.Now.ToString("g");
            
        }
    }
}
