using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Tech_Labs.Pages
{
    public class ServicesModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            Message = "These are the services you'll find in Tech-Labs.";
        }
    }
}
